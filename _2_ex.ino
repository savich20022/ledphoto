 #define led_pin 2
#define sensor_pin A2
void setup() {
  pinMode(led_pin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if (analogRead(sensor_pin) <800)digitalWrite(led_pin, HIGH);
  else digitalWrite(led_pin, LOW);
}
